from setuptools import setup
from setuptools import find_packages

setup(
    name='crypto-analyze',
    version=0.1,
    description='Cryptocurrencies Analyze.',

    classifiers=[
        'Programming Language :: Python :: 2.7',
    ],

    keywords='rest api flask flask-restful',

    packages=find_packages(),

    install_requires=[
        'coloredlogs==9.0',
        'pymongo==3.6.1',
        'flask==1.0.2',
        'flask-restful==0.3.6',
        'Flask-PyMongo==0.5.2',
        'requests==2.9.1',
        'mysql_connector==2.1.6',
        'keras==2.2.2',
        'pandas==0.23.4',
        'numpy==1.14.5',
        'scikit-learn==0.19.2',
        'tensorflow==1.10.0',
        'keras==2.2.2',
        'h5py==2.8.0',
        'scipy==1.1.0',
        'matplotlib==2.2.3'
        ]
)
