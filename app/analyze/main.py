import training
import predict
import matplotlib.pyplot as plt


def main():
    trade = training.TrainingCoinInfo()
    coin = 'eth'
    #start_time = 1520398472 + 8640  # btc
    #start_time = 1521322952 # iot
    start_time = 1520545352 # eth
    end_time = start_time + 8640
    trade.load_base(start_time, end_time, coin)
    trade.create_deep_learing()
    loss, accuracy = trade.run_deep_learning(100)
    #predict_test = predict.PredictCoinInfo('btc')
    #predict_test._load_base(trade.x_test)
    #predict_test.run_deep_learning()

    plt.plot(trade.real_price_test, color='red', label='Preco real')
    plt.plot(trade.prediction, color='blue', label='Preco previsto')
    plt.title('Preco da Ethereum')
    plt.xlabel('Tempo')
    plt.ylabel('Valor Bitfinex')
    plt.legend()
    plt.savefig(coin + '_' + str(end_time) + '_predicao')
    print trade.prediction.mean() - trade.real_price_test.mean()
    print 'Loss: {}'.format(loss)
    print 'accuracy {}'.format(float(accuracy)*100)

main()
