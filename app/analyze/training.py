"""
    Training deep Learning
"""
import logging
import pandas as pd
import numpy as np
import json

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import EarlyStopping
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import ModelCheckpoint
from sklearn.preprocessing import MinMaxScaler
from app.storage.cryptodb import CryptodbMysql


class TrainingCoinInfo(CryptodbMysql):
    """
        Create and Training deep learning
    """
    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger("TRAINING-COININFO")
        super(TrainingCoinInfo, self).__init__(logger=self.logger,
                                               database='coininfo')
        self.coin = None
        self.base = None
        self.base_test = None
        self.x_test = None
        self.regression = None
        self.normalize = None
        self.normalize_predict = None
        self.feature_lenght = 3
        self.predict = []
        self.real_price = []
        self.json_deep_learning_config = None

        self.early_stopping = EarlyStopping(monitor = 'loss',
                                            min_delta = 1e-10,
                                            patience = 10,
                                            verbose = 1)
        self.reduce_plateau = ReduceLROnPlateau(monitor = 'loss',
                                                factor = 0.2,
                                                patience = 5,
                                                verbose = 1)
        self.model_checkpoint = ModelCheckpoint(filepath = 'pesos.h5',
                                                monitor = 'loss',
                                                save_best_only = True,
                                                verbose = 1)
        self.callbacks = [
                          self.early_stopping,
                          self.reduce_plateau,
                          #self.model_checkpoint
                          ]

    def load_base(self, start_time, end_time, coin):
        self.start_time = start_time
        self.coin = coin
        self.logger.info('Load base training.')
        self.base = pd.read_sql("select distinct tid, price, amount, operation, coin, timestamp\
                                 from trade \
                                 where coin=(select id from coin where initial='%s') \
                                 and timestamp>=%s and timestamp<%s\
                                 order by timestamp" % (self.coin, start_time, end_time),
                                 con=self.mysqldb)

        test_size = int(len(self.base)*0.02)
        self.logger.info('Load base test.')
        self.base_test = pd.read_sql("select distinct tid, price, amount, operation,coin, timestamp\
                                      from trade \
                                      where coin=(select id from coin where initial='%s') \
                                      and tid > %s \
                                      order by timestamp \
                                      limit %s" % (self.coin, self.base['tid'][len(self.base) - 1], test_size),
                                      con=self.mysqldb)

        self.base.drop('tid', axis=1, inplace=True)
        self.base.drop('coin', axis=1, inplace=True)
        self.base.drop('timestamp', axis=1, inplace=True)
        self.base.dropna(inplace=True)

        self.base_test.drop('tid', axis=1, inplace=True)
        self.base_test.drop('coin', axis=1, inplace=True)
        self.base_test.drop('timestamp', axis=1, inplace=True)
        self.base_test.dropna(inplace=True)

        self.base_training = self.base.iloc[:, 0:self.feature_lenght].values
        self.real_price_test = self.base_test.iloc[:, 0:1].values
        self._initialize_normalize()

        self.timesteps = int(len(self.base) * 0.10)

        self.split_training_test()

    def _initialize_normalize(self):
        """
            Normalize base.
        """
        self.normalize = MinMaxScaler(feature_range=(0, 1))
        self.base_training_normalize = self.normalize.fit_transform(self.base_training)

        self.normalize_predict = MinMaxScaler(feature_range=(0, 1))
        self.normalize_predict.fit_transform(self.base_training[:,0:1])

    def split_training_test(self):
        """
             Split base into training and test.
        """
        for i in range(self.timesteps, len(self.base_training_normalize)):
            self.predict.append(self.base_training_normalize[i-self.timesteps:i, 0:self.feature_lenght])
            self.real_price.append(self.base_training_normalize[i, 0])

        self.predict = np.array(self.predict)
        self.real_price = np.array(self.real_price)

        self.base_complet = [self.base, self.base_test]
        self.base_complet = pd.concat(self.base_complet)

        self.entry = self.base_complet[len(self.base_complet) - len(self.base_test) - self.timesteps:].values
        self.entry = self.normalize.transform(self.entry)

        self.x_test = []

        for i in range(self.timesteps, len(self.entry)):
            self.x_test.append(self.entry[i - self.timesteps:i, 0:self.feature_lenght])

        self.x_test = np.array(self.x_test)

    def create_deep_learing(self):
        """
        Create neural network.
        """
        self.regression = Sequential()
        self.regression.add(LSTM(units=100, return_sequences=True, input_shape=(self.predict.shape[1], self.feature_lenght)))
        self.regression.add(Dropout(0.3))

        self.regression.add(LSTM(units=50, return_sequences=True))
        self.regression.add(Dropout(0.3))

        self.regression.add(LSTM(units=50, return_sequences=True))
        self.regression.add(Dropout(0.3))

        self.regression.add(LSTM(units=50))
        self.regression.add(Dropout(0.3))

        self.regression.add(Dense(units=1, activation='sigmoid'))
        self.regression.compile(optimizer='rmsprop',
                                loss='mean_squared_error',
                                metrics=['mean_absolute_error'])

    def save_deep_learning(self):
        self.regression.save_weights(self.coin +  '_' + str(self.start_time) + '_weight_deep_learning.h5')
        self.json_deep_learning_config = self.regression.to_json()

        with open(str(self.start_time) + '_' + self.coin + '_deep_learning_config.json', 'w') as json_file:
             json.dump(self.json_deep_learning_config, json_file, indent=2)

    def run_deep_learning(self, epochs):
        """
            Run neural network
        """
        print 'base'
        print len(self.base)
        #print self.base
        print '#'*80
        print 'base_test'
        print len(self.base_test)
        #print self.base_test
        print '#'*80
        print 'real price'
        print len(self.real_price)
        print '#'*80
        print 'test price'
        print len(self.real_price_test)
        print '#'*80
        print 'predict'
        print len(self.predict)
        print '#'*80
        print 'x_test'
        print len(self.x_test)
        print '#'*80
        print 'entry'
        print len(self.entry)
        self.regression.fit(self.predict, self.real_price, epochs=epochs,
                            batch_size=32,callbacks=self.callbacks)
        self.prediction = self.regression.predict(self.x_test)
        self.prediction = self.normalize_predict.inverse_transform(self.prediction)
        self.save_deep_learning()

        return self.regression.evaluate(self.predict, self.real_price)
