import logging
import numpy as np
import pandas as pd
import json
from keras.models import model_from_json
from sklearn.preprocessing import MinMaxScaler
from app.storage.cryptodb import CryptodbMysql

class PredictCoinInfo(CryptodbMysql):
    def __init__(self, coin):
        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger("PREDICT-COININFO")
        super(PredictCoinInfo, self).__init__(logger=logger,
                                               database='coin_realtime')
        self.coin = coin
        self.json_deep_learning_config = None
        self.prediction = None
        self._load_deep_learning()
        #self._load_base()

    def _load_base(self, test):
        print test

        self.base_test = test
        return
        self.base_test = pd.read_sql("select distinct tid, price, amount, operation,coin, timestamp\
                                      from trade \
                                      where coin=(select id from coin where initial='%s') \
                                      and tid > %s \
                                      order by timestamp \
                                      limit 20" % (self.coin, self.base['tid'][len(self.base) - 1]),
                                      con=self.mysqldb)

        self.base_test.drop('tid', axis=1, inplace=True)
        self.base_test.drop('coin', axis=1, inplace=True)
        self.base_test.drop('timestamp', axis=1, inplace=True)
        self.base_test.dropna(inplace=True)
        self.normalize_predict = MinMaxScaler(feature_range=(0, 1))
        self.normalize_predict.fit_transform(self.base_test[:,0:1])


    def _load_deep_learning(self):
        with open(self.coin + '_deep_learning_config.json', 'r') as json_file:
            self.json_deep_learning_config = json.load(json_file)
        self.prediction = model_from_json(self.json_deep_learning_config)
        self.prediction.load_weights(self.coin + '_weight_deep_learning.h5')

    def run_deep_learning(self):
        self.result = self.prediction.predict(self.base_test)
        self.result = self.normalize_predict.inverse_transform(self.prediction)
