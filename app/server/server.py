"""
    Start REST API endpoints server.
"""
from flask import Flask
from flask_restful import Api
from app.server.endpoints.trade import RequestTrade
from app.server.endpoints.current import CurrentValue
from app.server.endpoints.percentage import Percentage


class Server(object):
    """
        Server
    """
    def __init__(self):
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.init_routes()

    def init_routes(self):
        """
            Add routes by endpoints.
        """
        url_trade = '/coininfo/trade'
        url_current = '/coininfo/current'
        url_percentage = '/coininfo/percentage'

        self.api.add_resource(RequestTrade,
                              url_trade)
        self.api.add_resource(CurrentValue,
                              url_current)
        self.api.add_resource(Percentage,
                              url_percentage)

    def start(self):
        """
            Start server.
        """
        self. app.run(host="localhost", port=8080)
