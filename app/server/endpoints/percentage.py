import datetime
from flask import Flask
from flask import Response
from flask import abort
from flask import json
from flask import request
from flask_restful import Resource
from app.storage.cryptodb import CoinInfo

class Percentage(Resource):

    def __init__(self):
        self.coininfo = CoinInfo()

    def get(self):
        coin = request.args.get('coin')
        minutes = request.args.get('minute')
        live_update = {}

        try:
            minutes = int(minutes)
        except ValueError as error:
            abort(404, error.message)

        if not self.coininfo.is_coin(coin):
            abort(404, 'Invalid coin!')

        live_update['value'] = self.coininfo.get_current_value(coin)

        now = datetime.datetime.now()
        date_ago = now - datetime.timedelta(minutes=minutes)
        date_ago = int(date_ago.strftime('%s'))

        live_update['up'], live_update['percentage'] = self.coininfo.get_percentagem_up(coin, date_ago, live_update['value'])

        return Response(json.dumps(live_update), mimetype='application/json')
