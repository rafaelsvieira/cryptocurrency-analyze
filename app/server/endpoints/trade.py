import datetime
from flask import Flask
from flask import Response
from flask import abort
from flask import json
from flask import request
from flask_restful import Resource
from app.storage.cryptodb import CoinInfo

class RequestTrade(Resource):

    def __init__(self):
        self.coininfo = CoinInfo()

    def get(self):
        coin = request.args.get('coin')
        days = request.args.get('days')
        try:
            days = int(days)
        except ValueError as error:
            abort(404, error.message)

        if not self.coininfo.is_coin(coin):
            abort(404, 'Invalid coin!')

        now = datetime.datetime.now()
        date_ago = now - datetime.timedelta(days=days)
        date_ago = int(date_ago.strftime('%s'))
        trader_list = self.coininfo.get_trade(coin, date_ago)

        return Response(json.dumps(trader_list), mimetype='application/json')
