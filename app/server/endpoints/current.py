import datetime
from flask import Flask
from flask import Response
from flask import abort
from flask import json
from flask import request
from flask_restful import Resource
from app.storage.cryptodb import CoinInfo

class CurrentValue(Resource):

    def __init__(self):
        self.coininfo = CoinInfo()

    def get(self):
        coin = request.args.get('coin')
        print '*'*80
        print coin
        print '*'*80

        live_update = {}
        live_update['value'] = self.coininfo.get_current_value(coin)

        return Response(json.dumps(live_update), mimetype='application/json')
