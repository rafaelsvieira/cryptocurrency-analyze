"""
    Get infos from exchanges
"""
import time
import os
import logging
import requests
import coloredlogs  # pylint: disable=import-error
from  app.storage.cryptodb import CoinInfo


class RequestData(object):
    """
        Operations to get infos from exchanges rest api.
    """
    def __init__(self, logger=None):

        self.logdir = os.path.join(os.getcwd(), '..', '..')

        if not logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("COIN")
        coloredlogs.install(level='DEBUG')
        coloredlogs.install(level='DEBUG', logger=self.logger)

        self.cryptodb = CoinInfo()

    def get_json(self, url, coin):
        """
            Get json from exchange rest api.
        """
        self.logger.info("[%s] %s", coin, time.strftime("%H:%M:%S"))
        retrieve = 0
        max_retrieve = 5

        while True:
            try:
                response = requests.request("GET", url)

                if response.status_code == 200:
                    return response.json()
                elif response.status_code == 503:
                    self.logger.error("%s: Not inserting!Error %s\n%s\nWaiting for 5 min.",
                                      coin, response.status_code,
                                      response.json()['error_description'])
                    time.sleep(300)
                    return None
                else:
                    self.logger.error("%s: Not inserting! error(%s)\nWaiting for 10 sec.",
                                      coin, response.status_code)
                    time.sleep(10)
                    return None
            except requests.ConnectionError as error:
                if retrieve == max_retrieve:
                    self.print_logger(error, time.strftime("%H:%M:%S"))
                    raise

                self.logger.error("%s[%s]: %s", coin, retrieve, error)
                self.logger.error("%s: Connection error.\nWaiting for 30 sec (retrieve(%s/%s).",
                                  coin, retrieve, max_retrieve)
                time.sleep(30)
                retrieve += 1
            except Exception as error:  # pylint: disable=broad-except
                if retrieve == max_retrieve:
                    self.print_logger(error, time.strftime("%H:%M:%S"))
                    raise

                self.logger.error("%s[retrive(%s/%s)]: %s", coin, retrieve, max_retrieve,
                                  error.message)
                retrieve += 1

    def print_logger(self, msg, msgtime):
        """
            Show log in console.
        """
        log_dir = os.path.join(self.logdir, "logs")
        if not os.path.isdir(log_dir):
            os.makedirs(log_dir)
        logger_file = os.path.join(log_dir, ("log" + time.strftime("%Y%m%d") + ".txt"))
        with open(logger_file, "w") as output:
            output.write(str(msgtime) + ": " + str(msg.message))

