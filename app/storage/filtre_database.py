import datetime
from app.storage.cryptodb import CoinInfo

info = CoinInfo()
date_ago = info.get_next_time(0, 'btc')
now = date_ago + 60
last_time = info.get_last_time('btc')

while date_ago <= last_time:
    data = info.get_trade_between(now, date_ago, 'btc')
    now += 60
    date_ago +=60
    if data:
        info.insert_transaction(data)
    else:
        date_ago = info.get_next_time(date_ago, 'btc')
        now = date_ago + 60

"""
id | abertura | fechamento | quantidade comprada | quantidade vendida | maior | menor
"""
