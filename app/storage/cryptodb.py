"""
 Storage datas
"""
import datetime
import logging
import pymongo
import mysql.connector


class Cryptodb(object):  # pylint: disable=too-few-public-methods
    """
        Instances to mongodb.
    """
    def __init__(self, logger=None):
        self.localclient = pymongo.MongoClient('localhost', 27017)

        if logger is None:
            logging.basicConfig(level=logging.INFO)
            self.logger = logging.getLogger("COIN-DB")


class CryptodbMysql(object):
    """
        Instances to mysql.
    """
    def __init__(self,
                 logger=None,
                 host='localhost',
                 user='root',
                 password='rafa1234',
                 database='coin_realtime'):
        self.mysqldb = mysql.connector.connect(host=host,
                                               user=user,
                                               password=password,
                                               database=database)
        self.mysql = self.mysqldb.cursor(buffered=True)

        if logger is None:
            logging.basicConfig(level=logging.INFO)
            self.logger = logging.getLogger("COIN-DB")


class CoinInfo(CryptodbMysql):
    """
        Operation to db coininfo.
    """
    def __init__(self):
        super(CoinInfo, self).__init__()

        self.current_time = 0
        self.next_time = 0
        self.trade_time = 0
        self.forward_time = 60

        self._init_time();

        self.transaction_list = []



    def _init_time(self):
        self.next_time = self.get_last_time('btc')
        self.trade_time = self._get_current_time() + self.forward_time

    @staticmethod
    def _get_current_time():
        return int(datetime.datetime.now().strftime("%s"))

    def is_coin(self, coin):
        """
            Check if coin exists
        """
        query = "(select id from coin where initial='%s')" % coin

        self.mysql.execute(query)
        if self.mysql is ():
            return False
        else:
            return True

    def get_trade(self, coin, timestamp):
        """
            Get coin trades.
        """
        trade_list = []
        query = "select distinct id, tid, timestamp, \
                price from trade where  \
                coin=(select id from coin where initial='%s')  \
                and timestamp>%s order by id desc" % (coin, timestamp)

        self.mysql.execute(query)

        for trade in reversed(tuple(self.mysql)):
            trade_aux = {}
            trade_aux['timestamp'], trade_aux['price'] = trade[2:]
            trade_list.append(trade_aux)

        return trade_list

    def get_trade_between(self, now, date_ago, coin):
        """
            Get trades between a time.
        """
        trade_list = []
        trade_aux = {}

        query = "select distinct tid, price, amount, operation, coin, timestamp \
                 from trade \
                 where coin=(select id from coin where initial='%s') and \
                 timestamp>%s and timestamp<%s" % (coin, date_ago, now)

        self.mysql.execute(query)

        for trade in reversed(tuple(self.mysql)):
            trade_aux = {}
            trade_aux['tid'], trade_aux['price'], \
            trade_aux['amount'], trade_aux['operation'] = trade[:4]
            trade_aux['coin'] = coin
            trade_list.append(trade_aux)
        print trade_aux
        if trade_list:
            return self._convert_transaction(trade_list)

    @staticmethod
    def _convert_transaction(trade_list):
        new_list = {}
        new_list['amount_buy'] = 0
        new_list['amount_sell'] = 0
        if len(trade_list) > 0:
            new_list['tid_open'] = trade_list[0]['tid']
            new_list['tid_close'] = trade_list[-1]['tid']
            new_list['open'] = trade_list[0]['price']
            new_list['close'] = trade_list[-1]['price']
            new_list['bigger'] = trade_list[0]['price']
            new_list['smaller'] = trade_list[0]['price']
            new_list['coin'] = trade_list[0]['coin']
        else:
            return

        for value in trade_list:
            if value['operation'] == 1:
                new_list['amount_buy'] += float(value['amount'])
            else:
                new_list['amount_sell'] += float(value['amount'])

            if value['price'] > new_list['bigger']:
                new_list['bigger'] = value['price']

            if value['price'] < new_list['smaller']:
                new_list['smaller'] = value['price']

            if value['tid'] < new_list['tid_open']:
                new_list['tid_open'] = value['tid']

            if value['tid'] > new_list['tid_close']:
                new_list['tid_close'] = value['tid']

        new_list['amount_operations'] = len(trade_list)

        return new_list

    def get_percentagem_up(self, coin, timestamp, compare):
        """
            Get percentagem value.
        """
        query = "select price from \
                (select timestamp, price from trade \
                where coin=(select id from coin where initial='%s') \
                order by id desc) trade \
                where timestamp <%s limit 1" % (coin, timestamp)

        self.mysql.execute(query)

        current = tuple(self.mysql)[0][0]

        percentagem = ((compare/current) - 1) * 100

        if percentagem > 0:
            percentagem_up = True
        else:
            percentagem = percentagem * -1
            percentagem_up = False

        return (percentagem_up, percentagem)

    def get_current_value(self, coin):
        """
            Get current value.
        """
        query = "select price from trade \
                where coin=(select id from coin where initial='%s') \
                order by id desc limit 1" % (coin)
        print '#'*80
        print query
        print '#'*80
        self.mysql.execute(query)

        return tuple(self.mysql)[0][0]

    def insert_orderbook(self, jsondata):
        """
            Insert into orderbook collection.
        """
        self.logger.info("LOCAL-ORDERBOOK: Inserting...")

        if jsondata is not list:
            jsondata = [jsondata]

        for info in jsondata:
            for bid in info['bids']:
                query = "insert into orderbook(exchange, timestamp, price, amount, coin, request)\
	    		  values(\
	    		  (select id from exchange where name=%s),\
	    		  %s,\
	    		  %s,\
	    		  %s,\
	    		  (select id from coin where initial=%s),\
	    		  (select id from request where type=%s))"

                values = (info['exchange'], bid['timestamp'], bid['price'], bid['amount'],
                          info['coin'], 'bid')
                self.mysql.execute(query, values)

            for ask in info['asks']:
                query = "insert into orderbook(exchange, timestamp, price, amount, coin, request)\
	    		  values(\
	    		  (select id from exchange where name=%s),\
	    		  %s,\
	    		  %s,\
	    		  %s,\
	    		  (select id from coin where initial=%s),\
	    		  (select id from request where type=%s))"

                values = (info['exchange'], ask['timestamp'], ask['price'], ask['amount'],
                          info['coin'], 'ask')
                self.mysql.execute(query, values)

        self.mysqldb.commit()

        self.logger.info("LOCAL: Insert OK!")

    def insert_trade(self, jsondata):
        """
            insert into trade collection.
        """
        #self.logger.info("LOCAL-TRADE: Inserting...")

        if not isinstance(jsondata, list):
            jsondata = [jsondata]

        for info in jsondata:
            query = "insert into trade(exchange, timestamp, price, amount, tid, coin, operation)\
	    	  values(\
	    	  (select id from exchange where name=%s),\
	    	  %s,\
	    	  %s,\
	    	  %s,\
	    	  %s,\
	    	  (select id from coin where initial=%s),\
	    	  (select id from operation where type=%s))"

            if not info['exchange']:
                info['exchange'] = 'bitfinex'

            values = (info['exchange'], info['timestamp'], info['price'], info['amount'],
                      info['tid'], info['coin'], info['type'])

            self.mysql.execute(query, values)

        self.mysqldb.commit()
        self.logger.info("LOCAL-TRADE: Insert OK!")
        self.insert_trade_transaction()

    def insert_trade_transaction(self):
        """
            insert into trade collection.
        """
        current_time = self._get_current_time()
        if self.trade_time < current_time:
            self.logger.info("LOCAL-TRANSACTION: Inserting...")
            jsondata = self.get_trade_between(self.next_time, self.next_time - self.forward_time , 'btc')
            if jsondata:
                self.insert_transaction(jsondata)
                self._init_time()
            else:
                self.next_time += self.forward_time

    def insert_transaction(self, jsondata):
        """
            Insert transaction info into transaction table.
        """
        query = "insert into transaction\
        (tid_open, tid_close, open, close, amount_buy, amount_sell, bigger, smaller, amount_operations, coin)\
		values(%s, %s, %s, %s, %s, %s, %s, %s, %s, (select id from coin where initial=%s))"


        values = (jsondata['tid_open'], jsondata['tid_close'], jsondata['open'], jsondata['close'], jsondata['amount_buy'], jsondata['amount_sell'],
                  jsondata['bigger'], jsondata['smaller'], jsondata['amount_operations'], jsondata['coin'])

        self.mysql.execute(query, values)
        self.mysqldb.commit()

    def get_next_time(self, date, coin):
        """
            Get next time.
        """
        query ="select timestamp, coin from trade where timestamp>={} and coin=(select id from coin where initial=\'{}\') order by timestamp limit 1".format(date, coin)

        self.mysql.execute(query)
        self.mysqldb.commit()

        return int(tuple(self.mysql)[0][0])

    def get_last_time(self, coin):
        """
            Get last time.
        """
        query ="select timestamp, coin from trade where coin=(select id from coin where initial=\'{}\') order by timestamp desc limit 1".format(coin)

        self.mysql.execute(query)
        self.mysqldb.commit()

        return tuple(self.mysql)[0][0]
