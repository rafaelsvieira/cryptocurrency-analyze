"""
    Get infos from Bitfinex
"""
import logging
from  app.storage.request_data import RequestData
from time import sleep

class BitfinexInfo(RequestData):
    """
        Get infos from Bitfinex.
    """
    def __init__(self, logger=None):
        super(BitfinexInfo, self).__init__(logger)
        if not logger:
            self.logger = logging.getLogger("COIN-Bitfinex")

        self.coin_list = ['btc', 'eth', 'iot']

        self.urls = {}
        self.urls['orderbook'] = {}
        self.urls['trade'] = {}

        self.urls['orderbook']['btc'] = "https://api.bitfinex.com/v1/book/btcusd"
        self.urls['trade']['btc'] = "https://api.bitfinex.com/v1/trades/btcusd"

        self.urls['orderbook']['iot'] = "https://api.bitfinex.com/v1/book/iotusd"
        self.urls['trade']['iot'] = "https://api.bitfinex.com/v1/trades/iotusd"

        self.urls['orderbook']['eth'] = "https://api.bitfinex.com/v1/book/ethusd"
        self.urls['trade']['eth'] = "https://api.bitfinex.com/v1/trades/ethusd"

    def insert_orderbook(self, coin):
        """
            Insert orderbooks.
        """
        data = self.get_json(self.urls['orderbook'][coin], coin.upper())
        if data:
            data["exchange"] = "bitfinex"
            data["coin"] = coin
            self.cryptodb.insert_orderbook(data)

    def insert_trade(self, coin):
        """
            Insert trades.
        """
        data = self.get_json(self.urls['trade'][coin], coin.upper())
        data_aux = []
        if data:
            for count in range(0, len(data)):
                #data_aux.append(trade)
                data[count]['coin'] = coin
                data[count]['exchange'] = 'bitfinex'
            self.cryptodb.insert_trade(data)

    def insert_all_collections(self):
        """
            Insert all collections
        """
        for coin in self.coin_list:
            self.insert_trade(coin)
            sleep(2)
            #self.insert_orderbook(coin)
