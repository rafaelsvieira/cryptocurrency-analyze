"""
    Get infos from Bitfinex
"""
import logging
from  app.storage.data_storage import DataStorage


class BinanceInfo(DataStorage):
    def __init__(self):
        super(BinanceInfo, self).__init__()
        self.logger = logging.getLogger("COIN-Binance")
        self.btcurlorderbook = ""
        self.ioturlorderbook = ""
        self.ethurlorderbook = ""

    def insert_orderbook_btc(self):
        data = self.get_json(self.btcurlorderbook, "BTC")
        if data is not None:
            data["exchange"] = "bitfinex"
            data["coin"] = "btc"
            self.cryptodb.insert_orderbook(data)

    def insert_orderbook_eth(self):
        data = self.get_json(self.ethurlorderbook, "ETH")
        if data is not None:
            data["exchange"] = "bitfinex"
            data["coin"] = "eth"
            self.cryptodb.insert_orderbook(data)

    def insert_orderbook_iot(self):
        data = self.get_json(self.ioturlorderbook, "IOT")
        if data is not None:
            data["exchange"] = "bitfinex"
            data["coin"] = "iot"
            self.cryptodb.insert_orderbook(data)

