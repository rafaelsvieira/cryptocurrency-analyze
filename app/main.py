from app.server.server import Server

def init_server():
    server = Server()
    server.start()

def main():
    init_server()

if __name__ == "__main__":
    main()
