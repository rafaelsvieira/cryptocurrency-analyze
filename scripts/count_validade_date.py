import mysql.connector
import pandas as pd

def get_size_datas(mysqldb, coin):
    start_time=1520286152
    end_time = start_time + 86400
    finish_time = 1525269565


    while start_time < finish_time:
        base = pd.read_sql("select distinct tid, price, amount, operation, coin, timestamp\
        from trade \
        where coin=(select id from coin where initial='%s') \
        and timestamp>=%s and timestamp<%s\
        order by timestamp" % (coin, start_time,
        end_time), con=mysqldb)
        str_file = '------------------------\n'
        str_file += 'start_time: {}\n'.format(start_time)
        str_file += 'end_time: {}\n'.format(end_time)
        str_file += 'total:{}\n'.format(len(base))
        str_file += '------------------------\n'
        print str_file
        with open (coin + '_result_data.txt', 'a') as output:
            output.write(str_file)
            start_time += 86400
            end_time += 86400

def main():
    mysqldb = mysql.connector.connect(host='localhost',
                                        user='root',
                                        password='rafa1234',
                                        database='coin_realtime')
    coins = ['btc', 'iot', 'eth']

    for coin in coins:
        get_size_datas(mysqldb, coin)

main()
