"""
    Module convert datas mongo to mysql.
"""
import csv
import mysql.connector
import pymongo
from bson.objectid import ObjectId

class DBclass(object):
    """
        convert datas
    """
    def __init__(self):
        mongo = pymongo.MongoClient('localhost', 27017)
        self.mongo = mongo.coininfo
        self.mysqldb = mysql.connector.connect(host='localhost',
                                               user='root',
                                               password='rafa1234',
                                               database='coininfo')
        self.mysql = self.mysqldb.cursor()
        self.create_all_tables()
        self.insert_tables()

    def get_document_trade(self, doc_id):
        """
            Get mongo id document
        """
        return self.mongo.trade.find({"_id": ObjectId(doc_id)}).limit(1)[0]

    def get_document_orderbook(self, doc_id):
        """
            Get mongo id document
        """
        return self.mongo.orderbook.find({"_id": ObjectId(doc_id)}).limit(1)[0]

    def insert_orderbook(self, info):
        """
            Insert orderbook into table.
        """

        for bid in info['bids']:
            query = "insert into orderbook(exchange, timestamp, price, amount, coin, request)\
			  values(\
			  (select id from exchange where name=%s),\
			  %s,\
			  %s,\
			  %s,\
			  (select id from coin where initial=%s),\
			  (select id from request where type=%s))"

            values = (info['exchange'], bid['timestamp'], bid['price'], bid['amount'],
                      info['coin'], 'bid')
            self.mysql.execute(query, values)

        for ask in info['asks']:
            query = "insert into orderbook(exchange, timestamp, price, amount, coin, request)\
			  values(\
			  (select id from exchange where name=%s),\
			  %s,\
			  %s,\
			  %s,\
			  (select id from coin where initial=%s),\
			  (select id from request where type=%s))"

            values = (info['exchange'], ask['timestamp'], ask['price'], ask['amount'],
                      info['coin'], 'ask')
            self.mysql.execute(query, values)

        self.mysqldb.commit()

    def insert_trade(self, info):
        """
            Insert trade info into trade table.
        """
        query = "insert into trade(exchange, timestamp, price, amount, tid, coin, operation)\
		  values(\
		  (select id from exchange where name=%s),\
		  %s,\
		  %s,\
		  %s,\
		  %s,\
		  (select id from coin where initial=%s),\
		  (select id from operation where type=%s))"

        if not info['exchange']:
            info['exchange'] = 'bitfinex'

        values = (info['exchange'], info['timestamp'], info['price'], info['amount'],
                  info['tid'], info['coin'], info['type'])

        self.mysql.execute(query, values)
        self.mysqldb.commit()

    def create_all_tables(self):
        """
            Create data base and all tables.
        """
        self.mysql.execute("DROP TABLE IF EXISTS trade")
        self.mysql.execute("DROP TABLE IF EXISTS orderbook")
        self.mysql.execute("DROP TABLE IF EXISTS exchange")
        self.mysql.execute("DROP TABLE IF EXISTS coin")
        self.mysql.execute("DROP TABLE IF EXISTS operation")
        self.mysql.execute("DROP TABLE IF EXISTS request")

        self.mysql.execute("create table exchange (\
		  id int primary key auto_increment not null,\
		  name varchar(20) not null)")

        self.mysql.execute("create table coin (\
		  id int primary key auto_increment not null,\
		  name varchar(20) not null,\
		  initial varchar(3) not null)")

        self.mysql.execute("create table operation (\
		  id int primary key auto_increment not null,\
		  type varchar(10) not null)")

        self.mysql.execute("create table request (\
		  id int primary key auto_increment not null,\
		  type varchar(10))")

        self.mysql.execute("create table trade (\
		  id int primary key auto_increment not null,\
		  timestamp int not null,\
		  price double not null,\
		  amount double not null,\
		  tid int not null,\
		  exchange int not null,\
		  operation int not null,\
		  coin int not null,\
		  foreign key(exchange) references exchange(id),\
		  foreign key(operation) references operation(id),\
		  foreign key(coin) references coin(id))")

        self.mysql.execute("create table orderbook (\
		  id int primary key auto_increment not null,\
		  timestamp int not null,\
		  price double not null,\
		  amount double not null,\
		  exchange int not null,\
		  request int not null,\
		  coin int not null,\
		  foreign key(exchange) references exchange(id),\
		  foreign key(request) references request(id),\
		  foreign key(coin) references coin(id))")

    def insert_tables(self):
        """
            Insert tables;
        """
        query = "insert into exchange(name) values(\"bitfinex\")"
        self.mysql.execute(query)
        query = "insert into coin(name,initial) values(%s, %s)"
        values = ("Bitcoin", "btc")
        self.mysql.execute(query, values)
        query = "insert into coin(name,initial) values(%s, %s)"
        values = ("Iot", "iot")
        self.mysql.execute(query, values)
        query =	"insert into coin(name,initial) values(%s, %s)"
        values = ("Ethereum", "eth")
        self.mysql.execute(query, values)
        query =	"insert into operation(type) values(\"buy\")"
        self.mysql.execute(query)
        query =	"insert into operation(type) values(\"sell\")"
        self.mysql.execute(query)
        query =	"insert into request(type) values(\"ask\")"
        self.mysql.execute(query)
        query =	"insert into request(type) values(\"bid\")"
        self.mysql.execute(query)

        self.mysqldb.commit()

def main():
    """
        Main
    """
    db_convert = DBclass()
    with open('/media/rvieira/Data/FilesUbuntu/coininfo.trade.csv', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            result = ', '.join(row)
            if result == '"_id"':
                continue
            result = db_convert.get_document_trade(result)
            db_convert.insert_trade(result)

if __name__ == "__main__":
    main()
