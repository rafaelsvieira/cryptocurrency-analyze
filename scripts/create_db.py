"""
    Module convert datas mongo to mysql.
"""
import mysql.connector
import pymongo
from bson.objectid import ObjectId

class DBclass(object):
    """
        convert datas
    """
    def __init__(self):
        mongo = pymongo.MongoClient('localhost', 27017)
        self.mongo = mongo.coininfo
        self.mysqldb = mysql.connector.connect(host='localhost',
                                               user='root',
                                               password='rafa1234',
                                               database='coin_realtime')
        self.mysql = self.mysqldb.cursor()

    def get_document_trade(self, doc_id):
        """
            Get mongo id document
        """
        return self.mongo.trade.find({"_id": ObjectId(doc_id)}).limit(1)[0]

    def get_document_orderbook(self, doc_id):
        """
            Get mongo id document
        """
        return self.mongo.orderbook.find({"_id": ObjectId(doc_id)}).limit(1)[0]

    def insert_orderbook(self, info):
        """
            Insert orderbook into table.
        """

        for bid in info['bids']:
            query = "insert into orderbook(exchange, timestamp, price, amount, coin, request)\
			  values(\
			  (select id from exchange where name=%s),\
			  %s,\
			  %s,\
			  %s,\
			  (select id from coin where initial=%s),\
			  (select id from request where type=%s))"

            values = (info['exchange'], bid['timestamp'], bid['price'], bid['amount'],
                      info['coin'], 'bid')
            self.mysql.execute(query, values)

        for ask in info['asks']:
            query = "insert into orderbook(exchange, timestamp, price, amount, coin, request)\
			  values(\
			  (select id from exchange where name=%s),\
			  %s,\
			  %s,\
			  %s,\
			  (select id from coin where initial=%s),\
			  (select id from request where type=%s))"

            values = (info['exchange'], ask['timestamp'], ask['price'], ask['amount'],
                      info['coin'], 'ask')
            self.mysql.execute(query, values)

        self.mysqldb.commit()

    def insert_trade(self, info):
        """
            Insert trade info into trade table.
        """
        query = "insert into trade(exchange, timestamp, price, amount, tid, coin, operation)\
		  values(\
		  (select id from exchange where name=%s),\
		  %s,\
		  %s,\
		  %s,\
		  %s,\
		  (select id from coin where initial=%s),\
		  (select id from operation where type=%s))"
        values = (info['exchange'], info['timestamp'], info['price'], info['amount'],
                  info['tid'], info['coin'], info['type'])

        self.mysql.execute(query, values)
        self.mysqldb.commit()

    def insert_transaction(self, info):
        """
            Insert trade info into trade table.
        """
        query = "insert into transaction(open, close, amount_buy, amount_sell, bigger, smaller, coin)\
		  values(\
		  %s,\
		  %s,\
		  %s,\
		  %s,\
          %s,\
          %s,\
		  (select id from coin where initial=%s))"
        values = (info['open'], info['close'], info['amount_buy'], info['amount_sell'],
                  info['bigger'], info['smaller'], info['coin'])

        self.mysql.execute(query, values)
        self.mysqldb.commit()

    def create_all_tables(self, drop_tables=False):
        """
            Create data base and all tables.
        """
        if drop_tables is True:
            self._drop_all_tables()

        self._create_exchange_table()
        self._create_coin_table()
        self._create_operation_table()
        self._create_request_table()
        self._create_trade_table()
        self._create_orderbook_table()

    def _drop_all_tables(self):
        self.mysql.execute("DROP TABLE IF EXISTS trade")
        self.mysql.execute("DROP TABLE IF EXISTS orderbook")
        self.mysql.execute("DROP TABLE IF EXISTS exchange")
        self.mysql.execute("DROP TABLE IF EXISTS coin")
        self.mysql.execute("DROP TABLE IF EXISTS operation")
        self.mysql.execute("DROP TABLE IF EXISTS request")

    def _create_exchange_table(self):
        self.mysql.execute("create table exchange (\
		  id int primary key auto_increment not null,\
		  name varchar(20) not null)")

        self.mysqldb.commit()

    def _create_coin_table(self):
        self.mysql.execute("create table coin (\
		  id int primary key auto_increment not null,\
		  name varchar(20) not null,\
		  initial varchar(3) not null)")

        self.mysqldb.commit()

    def _create_operation_table(self):
        self.mysql.execute("create table operation (\
		  id int primary key auto_increment not null,\
		  type varchar(10) not null)")

        self.mysqldb.commit()

    def _create_request_table(self):
        self.mysql.execute("create table request (\
		  id int primary key auto_increment not null,\
		  type varchar(10))")

        self.mysqldb.commit()

    def _create_trade_table(self):
        self.mysql.execute("create table trade (\
		  id int primary key auto_increment not null,\
		  timestamp int not null,\
		  price double not null,\
		  amount double not null,\
		  tid int not null,\
		  exchange int not null,\
		  operation int not null,\
		  coin int not null,\
		  foreign key(exchange) references exchange(id),\
		  foreign key(operation) references operation(id),\
		  foreign key(coin) references coin(id))")

        self.mysqldb.commit()

    def _create_orderbook_table(self):
        self.mysql.execute("create table orderbook (\
		  id int primary key auto_increment not null,\
		  timestamp int not null,\
		  price double not null,\
		  amount double not null,\
		  exchange int not null,\
		  request int not null,\
		  coin int not null,\
		  foreign key(exchange) references exchange(id),\
		  foreign key(request) references request(id),\
		  foreign key(coin) references coin(id))")

        self.mysqldb.commit()

    def _create_transaction_table(self):
                self.mysql.execute("create table transaction (\
        		  id int primary key auto_increment not null,\
                  tid_open int not null,\
                  tid_close int not null,\
        		  open double not null,\
                  close double not null,\
        		  amount_buy double not null,\
                  amount_sell double not null,\
                  bigger double not null,\
                  smaller double not null,\
        		  coin int not null,\
                  amount_operations int not null,\
        		  foreign key(coin) references coin(id))")

                self.mysqldb.commit()

    def insert_tables(self):
        """
            Insert tables;
        """
        query = "insert into exchange(name) values(\"bitfinex\")"
        self.mysql.execute(query)
        query = "insert into coin(name,initial) values(%s, %s)"
        values = ("Bitcoin", "btc")
        self.mysql.execute(query, values)
        query = "insert into coin(name,initial) values(%s, %s)"
        values = ("Iot", "iot")
        self.mysql.execute(query, values)
        query =	"insert into coin(name,initial) values(%s, %s)"
        values = ("Ethereum", "eth")
        self.mysql.execute(query, values)
        query =	"insert into operation(type) values(\"buy\")"
        self.mysql.execute(query)
        query =	"insert into operation(type) values(\"sell\")"
        self.mysql.execute(query)
        query =	"insert into request(type) values(\"ask\")"
        self.mysql.execute(query)
        query =	"insert into request(type) values(\"bid\")"
        self.mysql.execute(query)

        self.mysqldb.commit()

def main():
    """
        Main
    """
    db_convert = DBclass()
    #db_convert.create_all_tables()
    #db_convert.insert_tables()
    db_convert._create_transaction_table()

if __name__ == "__main__":
    main()
