#!/bin/bash

HERE=$(dirname $(readlink -f "$0"))
ROOT_PATH=$(realpath -s $HERE/..)
SRC_PATH=$(realpath -s $HERE/../app)
VIRT_ENV=$(realpath -s $HERE/../.virt)

export PYTHONPATH=$PYTHONPATH:$ROOT_PATH

build_virtual_enviroment()
{
    VIRT_PATH=$1
    mkdir -p $VIRT_PATH
    virtualenv -p python2.7 $VIRT_PATH
    source $VIRT_PATH/bin/activate
    pip install -U pip
    pip install -U setuptools
}

run_application()
{
    build_virtual_enviroment $VIRT_ENV
    cd $VIRT_ENV
    python $ROOT_PATH/setup.py develop || exit 1
    python $SRC_PATH/main.py & python $SRC_PATH/api.py
}


run_application
